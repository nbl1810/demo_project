package Controller;

import Model.AuthModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/loginAuth")
public class LoginController extends HttpServlet {
    private AuthModel auth = new AuthModel();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String user = req.getParameter("username");
        String pass = req.getParameter("password");
        HttpSession session = req.getSession();
        RequestDispatcher rd = null;
        if (auth.authUser(user, pass)) {
            session.setAttribute("auth", "true");
        }else {
            session.setAttribute("error", "Username atau password salah!!");
        }
        if (session.getAttribute("auth") == "true") {
            resp.sendRedirect("/index");
        } else {
            resp.sendRedirect("/login");
        }
    }
}

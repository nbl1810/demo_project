<%
    if (session.getAttribute("auth") == null) response.sendRedirect("/login");

%>
<html>
<body>
    <h2>Selamat datang</h2>
    <form action="/input" method="post">
        <button type="submit">Tambah mahasiswa</button>
    </form>
    <table width="100%" border="1" cellspacing="0" cellpadding="10">
    	<tr>
    		<th align="center">No</th>
    		<th>Nama Mahasiswa</th>
    		<th>Alamat</th>
    		<th>Status</th>
    		<th>Nilai</th>
    		<th></th>
    	</tr>
    	<td align="center">1</td>
    	<td>Nabil</td>
    	<td>Raman</td>
    	<td>On Leave</td>
    	<td>
    		<ul>
    			<li>physics : 70</li>
    			<li>calculus : 70</li>
    			<li>biologi : 70</li>
    		</ul>
    	</td>
    	<td>
    		<ul>
    			<li>
    				<form action="/edit" method="post">
    					<button type="submit">Edit mahasiswa</button>
    				</form>
    			</li>
    			<li>
    				<form action="/delete" method="post">
    					<button type="submit">Hapus mahasiswa</button>
    				</form>
    			</li>
    		</ul>
    	</td>
    </table>
    <a href="/logout">logout</logout>
</body>
</html>

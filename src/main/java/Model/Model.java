package Model;
import Config.Mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Model {
    protected Model mysql = null;
    private Mysql connect = null;
    private String table = null;
    private String whereCond = null;

    public Model(){
        try {
            Connect();
            mysql = this;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    Mysql Connect() throws SQLException, ClassNotFoundException {
        if (connect == null) {
            this.connect = new Mysql("localhost", "root", "123", "day16");
            return this.connect;
        }else{
            return this.connect;
        }
    }

    Model table(String table) {
        this.table = table;
        return this;
    }

    ResultSet get() throws SQLException, ClassNotFoundException {
        if (whereCond == null) whereCond = "";
        ResultSet res = connect.exeQuery("SELECT*FROM " + table + " " + whereCond);

        return res;
    }

    Model where(String a, String b, String c) {
        whereCond = "WHERE " + a + b + "'" + c + "'";
        return this;
    }

    int count() {
        int i = 0;
        try {
            ResultSet res = get();
            while (res.next()) {
                i = i + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return i;
    }
}

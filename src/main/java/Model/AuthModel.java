package Model;

public class AuthModel extends Model{
    public AuthModel(){
        super();
    }

    public boolean authUser(String user, String pass) {
        boolean result = false;
        int userRes = mysql.table("user").where("username", "=", user).count();
        int passRes = mysql.table("user").where("password", "=", pass).count();
        if (userRes > 0 && passRes > 0) {
            result = true;
        }
        return result;
    }
}

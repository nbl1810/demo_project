package Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Mysql {
    private String HOST, Username, Password, Database;
    public Mysql(String HOST, String Username, String Password, String Database) throws ClassNotFoundException, SQLException {
        HOST = HOST;
        Username = Username;
        Password = Password;
        Database = Database;

    }

    public Connection con() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/day16?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "123");
    }

    public ResultSet exeQuery(String query) throws SQLException, ClassNotFoundException {
        ResultSet res = con().createStatement().executeQuery(query);
        closeConnection();
        return res;
    }

    public int execute(String query) throws SQLException, ClassNotFoundException {
        int res = con().createStatement().executeUpdate(query);
        closeConnection();
        return res;
    }

    public void closeConnection() throws SQLException, ClassNotFoundException {
        con().close();
    }
}
